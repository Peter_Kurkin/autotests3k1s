namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class WaitHelper : HelperBase
{
    public void Wait()
    {
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(3);
    }

    public WaitHelper(ApplicationManager manager)
        : base(manager)
    {
    }
}