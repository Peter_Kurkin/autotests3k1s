namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class NavigationHelper : HelperBase
{
    public void OpenHomePage()
    {
        driver.Navigate().GoToUrl(baseURL);
    }

    public void NavigateHomePage()
    {
        driver.FindElement(By.XPath(
                "/html/body/header/div/div[1]/a"))
            .Click();
    }
    
    private string baseURL;        
    public NavigationHelper(ApplicationManager manager, string baseURL)
        : base(manager)
    {
        this.baseURL = baseURL;
    }

}