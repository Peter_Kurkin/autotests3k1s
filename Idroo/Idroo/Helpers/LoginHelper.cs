namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class LoginHelper : HelperBase
{
    public void Auth(AccountData user)
    {
        if (IsLoggedIn())
        {
            if (IsLoggedIn(user))
            {
                return;
            }
            Logout();
        }
        
        driver.FindElement(By.LinkText("Login")).Click();
        driver.FindElement(By.Name("email")).SendKeys(user.Username);
        driver.FindElement(By.Name("password")).SendKeys(user.Password);
        driver.FindElement(By.CssSelector(".big")).Click();
        {
            var element = driver.FindElement(By.CssSelector(".big"));
            Actions builder = new Actions(driver);
            builder.MoveToElement(element).Perform();
        }
    }

    public void Logout()
    {
        driver.FindElement(By.XPath("//div/div/div[2]")).Click();
        driver.FindElement(By.XPath("/html/body/header/div/div[3]/div/div[2]/div/div")).Click();
    }

    public AccountData GetAuth()
    {
        string Username = driver.FindElement(By.Name("email")).GetAttribute("value");
        string Password = driver.FindElement(By.Name("password")).GetAttribute("value");
        // string header = driver.FindElement(By.Name("email")).Text;
        // string footer = driver.FindElement(By.Name("password")).Text;
        return new AccountData(Username, Password);
    }

    public LoginHelper(ApplicationManager manager)
        : base(manager)
    {
    }

    public bool IsLoggedIn()
    {
        try
        {
            var el = driver.FindElement(By.XPath("/html/body/header/div/div[3]/div"));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return false;
        }

        return true;
    }

    public bool IsLoggedIn(AccountData accountData)
    {
        string str = "Добро пожаловать, " + accountData.Username;
        IWebElement loginElement;
        try
        {
            loginElement = driver.FindElement(By.XPath("/html/body/header/div/div[3]/div"));
        }
        catch (Exception e)
        {
            return false;
        }

        string s = loginElement.Text;
        if (loginElement.Text.Equals(str))
        {
            return true;
        }

        return false;
    }
}