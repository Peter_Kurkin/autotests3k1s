namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class EditDeskHelper : HelperBase
{
    public void edit_desk(DeskData desk)
    {
        driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Last 7 days'])[1]/following::div[3]")).Click();
        driver.FindElement(By.XPath("//input[@type='text']")).Click();
        driver.FindElement(By.XPath("//input[@type='text']")).Clear();
        driver.FindElement(By.XPath("//input[@type='text']")).SendKeys(desk.Name);
        driver.FindElement(By.XPath("//input[@type='text']")).SendKeys(Keys.Enter);
        driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Download PDF'])[1]/preceding::*[name()='svg'][1]")).Click();
    }
    
    public DeskData GetNameDesk()
    {
        string Name = driver.FindElement(By.XPath("/html/body/div[1]/div/div/div/section/div/div/a/div[2]")).Text;
        return new DeskData(Name);
    }
    
    public EditDeskHelper(ApplicationManager manager)
        : base(manager)
    {
    }
}