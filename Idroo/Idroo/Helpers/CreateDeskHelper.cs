namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class CreateDeskHelper : HelperBase
{
    public void Create_desk()
    {
        driver.FindElement(
                By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Boards'])[2]/following::button[1]"))
            .Click();
    }
    
    public int GetNameDesk()
    {
        List<IWebElement> ToList = driver.FindElements(By.XPath("/html/body/div[1]/div/div/div/section/div")).ToList();
        int linkCount = ToList.Count;
        
        return ToList.Count;
    }
    
    public CreateDeskHelper(ApplicationManager manager)
        : base(manager)
    {
    }
}