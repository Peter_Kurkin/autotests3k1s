namespace Idroo;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;

public class ApplicationManager
{
    public IWebDriver driver;
    public string baseURL;
    public IDictionary<string, object> vars { get; private set; }
    public IJavaScriptExecutor js;

    private NavigationHelper navigation;
    private LoginHelper auth;
    private CreateDeskHelper createDesk;
    private WaitHelper wait;
    private EditDeskHelper edit_desk;

    private static ThreadLocal<ApplicationManager> app = new ThreadLocal<ApplicationManager>();

    public static ApplicationManager GetInstance()
    {
        if (!app.IsValueCreated)
        {
            ApplicationManager newInstance = new ApplicationManager();
            newInstance.navigation.OpenHomePage();
            app.Value = newInstance;
        }

        return app.Value;
    }

    private ApplicationManager()
    {
        driver = new FirefoxDriver();
        js = (IJavaScriptExecutor)driver;
        vars = new Dictionary<string, object>();
        baseURL = Settings.BaseURL;
        auth = new LoginHelper(this);
        navigation = new NavigationHelper(this, baseURL);
        createDesk = new CreateDeskHelper(this);
        wait = new WaitHelper(this);
        edit_desk = new EditDeskHelper(this);
    }

    ~ApplicationManager()
    {
        try
        {
            driver.Quit();
        }
        catch (Exception)
        {
            //ignore
        }
    }


    public void Stop()
    {
        driver.Quit();
    }

    public IWebDriver Driver
    {
        get { return driver; }
    }

    public EditDeskHelper EditDesk
    {
        get { return edit_desk; }
    }

    public NavigationHelper Navigation
    {
        get { return navigation; }
    }

    public LoginHelper Auth
    {
        get { return auth; }
    }

    public CreateDeskHelper CreateDesk
    {
        get { return createDesk; }
    }

    public WaitHelper WaitHelper
    {
        get { return wait; }
    }
}