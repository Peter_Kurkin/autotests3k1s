namespace Idroo;

using System;
using System.Collections.Generic;
using NUnit.Framework;
using System.Xml.Serialization;

[TestFixture]
public class DeserializeTest : TestBase
{
    XmlSerializer xmlSerializer = new XmlSerializer(typeof(DeskData));

    public static IEnumerable<DeskData>? NoteFromXmlFile()
    {
        return (List<DeskData>)new XmlSerializer(typeof(List<DeskData>))
            .Deserialize(new FileStream(@"/Users/patrick/Documents/Riders_Project/ConsoleApp1/ConsoleApp1/test.xml",
                FileMode.OpenOrCreate))!;
    }

    [Test, TestCaseSource(nameof(NoteFromXmlFile))]
    public void CreateNoteFromXmlTest(DeskData noteData)
    {
        String newData = noteData.Name;
        app.Navigation.OpenHomePage();
        AccountData user = new AccountData("peterkurkin1@gmail.com", "rojtif-suqBe2-tyggah");
        app.Auth.Auth(user);
        app.WaitHelper.Wait();
        app.CreateDesk.Create_desk();
        DeskData desk = new DeskData(newData);
        app.EditDesk.edit_desk(desk);
        app.Navigation.OpenHomePage();
        DeskData newDesk = app.EditDesk.GetNameDesk();
        app.Auth.Logout();
    }
}
