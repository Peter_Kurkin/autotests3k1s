namespace Idroo;

public class AuthBase : TestBase
{
    [SetUp]
    public void SetUp()
    {
        app = ApplicationManager.GetInstance();
        app.Navigation.OpenHomePage();
        AccountData user = new AccountData(Settings.Email, Settings.Password);
        app.Auth.Auth(user);
    }
}