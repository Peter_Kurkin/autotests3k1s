namespace Idroo;

public class DeskData
{
    public DeskData(){ }
    public DeskData(string name)
    {
        Name = name;
    }

    public string Name { get; set; }
}