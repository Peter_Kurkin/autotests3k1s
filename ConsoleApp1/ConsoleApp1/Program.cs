﻿using System.Xml;
using Bogus;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            // создаём объект класса Faker, указывая что необходимо
            // использовать русскую локализацию
            Console.WriteLine("НАПИШИ количество генерации городов");
            int count = Convert.ToInt32(Console.ReadLine());
            Faker faker = new Faker("ru");
            for (var x = 0; x < count; x++)
            {
                //Откуда загружаем
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load($"/Users/patrick/Documents/Riders_Project/ConsoleApp1/ConsoleApp1/test.xml");

                XmlElement xRoot = xDoc.DocumentElement;

                XmlElement groupdate = xDoc.CreateElement("DeskData");
                XmlElement place = xDoc.CreateElement("Name");
                XmlText citText = xDoc.CreateTextNode(faker.Address.Country());
                
                place.AppendChild(citText);
                groupdate.AppendChild(place);
                
                //Куда сохраняем
                xRoot.AppendChild(groupdate);
                xDoc.Save(@"/Users/patrick/Documents/Riders_Project/ConsoleApp1/ConsoleApp1/test.xml");
            }
        }
    }
}